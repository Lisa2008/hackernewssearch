import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display title', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Hacker News Topic Search');
  });

  it('should show search input and icon', () => {
    page.navigateTo();
    expect(page.getSearchInput()).toBeTruthy();
    expect(page.getSearchIcon()).toBeTruthy();
  });

  it('should not show search result at the beginning', () => {
    page.navigateTo();
    expect(page.getSearchResult().isDisplayed()).toBe(false);
    expect(page.getPreviousBtn().isDisplayed()).toBe(false);
  });

  it('should show results after sending query string', () => {
    page.navigateTo();
    expect(page.setSearchInput('virus')).toEqual('virus');

    page.sendQuery();
    expect(page.getSearchResult().isDisplayed()).toBe(true);
    expect(page.getPreviousBtn().isDisplayed()).toBe(true);
    expect(page.getPreviousBtn().isEnabled()).toBe(false);
    expect(page.getPageNumSpan().isDisplayed()).toBe(true);
  });

  it('should show page number input when click on the page number', () => {
    page.navigateTo();
    expect(page.setSearchInput('virus')).toEqual('virus');
    page.sendQuery();

    expect(page.getPageNumSpan().isDisplayed()).toBe(true);
    page.clickPageNumSpan();

    expect(page.getPageNumInput().isDisplayed()).toBe(true);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
