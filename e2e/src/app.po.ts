import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.css('.searchtitle')).getText() as Promise<string>;
  }

  getSearchInput() {
    return element(by.css('.form-control'));
  }

  getSearchIcon() {
    return element(by.id('searchbtn'));
  }

  getSearchResult() {
    return element(by.tagName('ul'));
  }

  getPreviousBtn() {
    return element(by.css('.btn-light'));
  }

  setSearchInput(topic: string) {
    const inputEl = this.getSearchInput();
    inputEl.sendKeys(topic)
    return inputEl.getAttribute('value');
  }

  sendQuery() {
    this.getSearchIcon().click();
  }

  getPageNumSpan() {
    return element(by.tagName('span'));
  }

  clickPageNumSpan() {
    this.getPageNumSpan().click();
  }

  getPageNumInput() {
    return element(by.css('[type="number"]'));
  }
}
