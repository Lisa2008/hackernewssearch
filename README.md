# Hackernews

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.

This is a search page of Hacker News (https://news.ycombinator.com/), which uses its search API (in https://hn.algolia.com/api)

# Details:

1. The search topic input, input a search topic, for example: "test", then type "Enter" or click the search icon, the results will show on the page.

2. I didn't do any validation for this input, since I tested, if the query is empty, such as "http://hn.algolia.com/api/v1/search?query=", there are still results returned, as well as single letter, such as "q", there are also results. So I just send the input to search API as it is.

3. Hacker News API default returns 20 items per page, so only 20 items are showed on the page.

4. The page number and total page is from the query response.

5. On the search result page, the page number starts from 1, so on the first page the "Previous" button is disabled, same when at the last page, the "Next" button is disabled.

6. When click on each result item, will open a new tab to show the article.

7. User can change page number manually by click on "Page 1 of 50" between "Previous" and "Next" button, and then input the page number, then press "Enter" or click "Go" button.

8. The page number input has set attribute min to 1 and max to the total page which is from the returned result. If the number inputed is less than the first page or larger than the last page, or empty, when press "Enter" or click "Go" button, no query will be sent, just show current page and quit editing page number.

9. This project uses Bootstrap styles.

10. Also included unit test and e2e test code, they are not thorough test, just a demostration.

# File structure:

This project is build by "ng new", so the file structure uses the default structure of Angluar.

src/app/app.component.html is the html 

src/app/app.component.css is the style file

src/app/app.component.ts is the Typescript code

src/app/search.service.ts is a service which sends query to Hacker News API and return results. 

For unit test

src/app/app.component.spec.ts is the unit test for app component

src/app/search.service.spec.ts is the unit test for search service

For e2e test

/e2e/src/app.e2e-spec.ts is the test code.

/e2e/src/app.po.ts is the support code.

# Run

Using "ng build" to build the project. The result is in 

dist\hackernews\

Need to deploy "hackernews" folder to a web server to run the application. 

A simple approach is using http-server or lite-server to run the result.

Here's how to run the project in http-server:

To install http-server (assuming that Node.js has setup):

npm install -g http-server

then go to the folder where "index.html" is ( dist\hackernews\ ), and run

http-server

or at anywhere run

http-server C:\path\to\app

then from a browser, type "http://127.0.0.1:8080/index.html".

# IDE

Using Visual Studio Code for this project. Can clone the project form Bitbucket

git clone https://Lisa2008@bitbucket.org/Lisa2008/hackernewssearch.git

If wanted to run this project,

run (assuming that Node.js installed)

npm install

in the working folder, then run

ng serve

to start a http server, then in a browser, type "http://localhost:4200/"

or run in Visual Studion Code IDE.

# Test

Need angular envirument, after get the souce code and run

npm install

Then in the work folder, run

ng test

to see the result of unit test.

Run

ng e2e

to see the result of e2e test.














1. 