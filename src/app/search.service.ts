import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private API_URL = 'https://hn.algolia.com/api';

  constructor(private httpClient: HttpClient) { }

  public getResult(searchStr: string): Promise<any> {
    const params = new HttpParams().set('query', searchStr);
    return this.httpClient.get(`${this.API_URL}/v1/search`, {params}).toPromise().catch(this.handleError);
  }

  public getResultByPage(searchStr: string, pageNumber: number): Promise<any> {
    let params = new HttpParams().set('query', searchStr);
    params = params.append('page', pageNumber.toString());
    return this.httpClient.get(`${this.API_URL}/v1/search`, {params}).toPromise().catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
