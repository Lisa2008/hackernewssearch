import { TestBed, async, ComponentFixture, tick, fakeAsync} from '@angular/core/testing';

import { AppComponent } from './app.component';
import { SearchService } from './search.service';

const testdata: any = require('../assets/testdata.json');
const secondPage: any = require('../assets/secondpage.json');

describe('Hacker News Search', () => {
  const searchServiceSpy = jasmine.createSpyObj('SearchService', ['getResult', 'getResultByPage']);
  let searchService: jasmine.SpyObj<any>;
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [{provide: SearchService, useValue: searchServiceSpy}]
    }).compileComponents();

    searchService = TestBed.inject(SearchService);

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
    expect(component.editPageNum).toBeFalse();
    expect(component.iferror).toBeFalse();
  });

  it('should send query to server when press enter or click the search button', () => {
    spyOn(component, 'startQuery');
    fixture.detectChanges();
    const searchInput = fixture.debugElement.nativeElement.querySelector('[type="text"]');
    searchInput.dispatchEvent(new KeyboardEvent('keyup', {'key': 'enter'}));
    expect(component.startQuery).toHaveBeenCalled();

    const searchbtn = fixture.debugElement.nativeElement.querySelector('#searchbtn');
    searchbtn.click();
    expect(component.startQuery).toHaveBeenCalled();
  });

  it('should process the result when server returns', fakeAsync(() => {
    searchService.getResult.and.returnValue( new Promise((resolve, reject) => {
      resolve(testdata);
    }));
    component.startQuery();

    fixture.detectChanges();
    tick();
    fixture.detectChanges();
    expect(searchService.getResult).toHaveBeenCalled();

    expect(component.searchResult.page).toEqual(0);
    expect(component.searchResult.results.length).toEqual(20);

    const resultul = fixture.debugElement.nativeElement.querySelector('.list-group');
    expect(resultul).toBeTruthy();
    const previousbtn = fixture.debugElement.nativeElement.querySelector('.btn-light');
    expect(previousbtn).toBeTruthy();
    expect(previousbtn.disabled).toBeTrue();
  }));

  it('should show error message when error happens', fakeAsync(() => {
    searchService.getResult.and.returnValue( new Promise((resolve, reject) => {
      reject('An error occurred');
    }));

    component.startQuery();

    fixture.detectChanges();
    tick();
    fixture.detectChanges();

    expect(component.iferror).toBeTrue();
    const errormsge = fixture.debugElement.nativeElement.querySelector('.alert-danger');
    expect(errormsge.innerHTML.indexOf('There are something wrong with server, please try again later.') !== -1).toBeTrue();
  }));

  it('should get result when query by page ', fakeAsync(() => {
    component.searchResult.results = testdata.hits;
    component.searchResult.page = testdata.page;
    component.searchResult.showpage = 1;

    searchService.getResultByPage.and.returnValue( new Promise((resolve, reject) => {
      resolve(secondPage);
    }));

    component.showNextPage();

    fixture.detectChanges();
    tick();
    fixture.detectChanges();

    expect(searchService.getResultByPage).toHaveBeenCalled();
    expect(component.searchResult.page).toEqual(1);
  }));

});
