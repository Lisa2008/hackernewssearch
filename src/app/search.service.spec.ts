import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { HttpClient, HttpParams } from '@angular/common/http';
import { SearchService } from './search.service';

const testdata: any = require('../assets/testdata.json');
const secondPage: any = require('../assets/secondpage.json');

describe('SearchService', () => {
  let service: SearchService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  const testUrl = '/api';


  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchService],
      imports: [ HttpClientTestingModule]
    });
    service = TestBed.inject(SearchService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send search query and get results', () => {
    const params = new HttpParams().set('query', 'virus');
    httpClient.get(testUrl, {params})
      .subscribe( data => {

      expect(data).toEqual(testdata);
    });

    const req = httpTestingController.expectOne(testUrl + '?query=virus');
    expect(req.request.method).toEqual('GET');
    expect(req.request.params.get('query')).toEqual('virus');
    req.flush(testdata);
  });

  it('should send search query by page number and get results', () => {
    let params = new HttpParams().set('query', 'virus');
    params = params.append('page', '1');
    httpClient.get(testUrl, {params})
      .subscribe( data => {

      expect(data).toEqual(secondPage);
    });

    const req = httpTestingController.expectOne(testUrl + '?query=virus&page=1');
    expect(req.request.method).toEqual('GET');
    expect(req.request.params.get('query')).toEqual('virus');
    expect(req.request.params.get('page')).toEqual('1');
    req.flush(secondPage);
  })
});
