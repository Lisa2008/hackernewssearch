import { Component } from '@angular/core';

import { SearchService } from './search.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  searchStr: string;
  searchResult = {results: [],
                  page: 0,
                  showpage: 0,
                  totalpage: 0};
  editPageNum = false;
  iferror = false;

  constructor(private searchService: SearchService) { }

  startQuery() {
    this.searchService.getResult(this.searchStr).then(result => {
      this.setResult(result);
    })
    .catch(error => {
      this.iferror = true;
    });
  }

  showPreviousPage() {
    if (this.searchResult.page === 0) return;
    this.searchPage(this.searchResult.page - 1);
  }

  showNextPage() {
    if ((this.searchResult.page + 1) === this.searchResult.totalpage) return;
    this.searchPage(this.searchResult.page + 1);
  }

  showSpecificPage() {
    let tempPage = this.searchResult.showpage;
    if (tempPage === null || tempPage <= 0 || tempPage - 1 >= this.searchResult.totalpage) {
      this.searchResult.showpage = this.searchResult.page + 1;
      this.editPageNum = false;
      return;
    }
    this.searchPage(tempPage - 1);
  }

  switchPageEdit() {
    this.editPageNum = true;
  }

  chagePage(newPageNumber: number) {
    this.searchResult.showpage = newPageNumber;
  }

  private searchPage(page: number) {
    this.searchService.getResultByPage(this.searchStr, page).then(result => {
      this.setResult(result);
    }).catch(error => {
      this.iferror = true;
    });
  }

  private setResult(result: any) {
    this.searchResult.results = result.hits;
    this.searchResult.page = result.page;
    this.searchResult.showpage = result.page + 1;
    this.searchResult.totalpage = result.nbPages;

    this.iferror = false;
    this.editPageNum = false;
  }

}
